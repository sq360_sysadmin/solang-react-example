import React from 'react';
import './App.css';

import './App.scss';
import {
  createApp,
  getAppFromState,
  processSimpleFilter, setParams
} from "solang-react/build/solang/store/solang.slice";
import { useAppDispatch, useAppSelector } from "./app/hooks";
import { RootState } from "./app/store";
import { ISolangParamList } from "solang-react/build/solang/solang.types";
import { TestSolang } from "./app/TestSolang/TestSolang";

function App() {

  const APP_ID = 'example_app';
  const dispatch = useAppDispatch();

  const searchApp = useAppSelector((state: RootState) => getAppFromState(state.solang, APP_ID) );

  if (!searchApp) {
    dispatch(createApp({
      id: APP_ID,
      endpoint: 'http://localhost:8983/solr/solang/',
      params: {
        searchText: 'Da'
      },
      filters: {
        searchText: {
          config: {
            solrField: 'first_name_t',
            alias: 'searchText',
          },
          processQueryActions: [processSimpleFilter.type],
          value: []
        },
      },
    }));
  }

  const getResults = (e: any) => {
    e.preventDefault();
    const params: ISolangParamList = {...searchApp.params};
    params.s = '*';
    dispatch(setParams({appId: APP_ID, params: params}));
  }

  return (
    <div className="App">
      <h1>Solang Example Application</h1>
      <button onClick={getResults}>Get Results</button>
      <TestSolang></TestSolang>
    </div>
  );
}

export default App;
