import React, { useState } from 'react';
import { useAppDispatch, useAppSelector } from "../hooks";
import { RootState } from "../store";
import { getAppFromState, setParams } from "solang-react/build/solang/store/solang.slice";
import { ISolangParamList } from "solang-react/build/solang/solang.types";


export const TestSolang = () => {

  const APP_ID = 'example_app';
  const FILTER_KEY = 'searchText';

  const dispatch = useAppDispatch();

  const searchApp = useAppSelector((state: RootState) => getAppFromState(state.solang, APP_ID) );

  const results = useAppSelector((state: RootState) => {
    return ((searchApp && searchApp.response && searchApp.response.response)) ? searchApp.response.response.docs : [];
  });


  const searchParameter = useAppSelector((state: RootState) => {
    const app = getAppFromState(state.solang, APP_ID);
    return app ? app.params[FILTER_KEY] : 'undefined';
  });

  const [getSearchString, setSearchString] = useState(searchParameter);

  const updateParams = (e: any) => {
    e.preventDefault();
    const params: ISolangParamList = {...searchApp.params};
    params[FILTER_KEY] = getSearchString;
    dispatch(setParams({appId: APP_ID, params: params}));
  }

  return (
    <div>
      <h2>Testing Redux Observables</h2>

      { results && (
        <ul>
          {results.map(item => (
            <li key={item.id}>{item.first_name_s} {item.last_name_s}</li>
          ))}
        </ul>
      )}

    </div>
  );
}
